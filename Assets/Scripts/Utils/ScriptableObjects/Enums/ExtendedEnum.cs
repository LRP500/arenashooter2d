﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ScriptableObjects.Enums
{
	/// <inheritdoc />
	/// <summary>
	/// Defines a dictionary of custom keys and values
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="U"></typeparam>
	public class ExtendedEnum<T, U> : SerializedScriptableObject where U : AExtendedEnumElement
	{
		[SerializeField]
		[DictionaryDrawerSettings(DisplayMode = DictionaryDisplayOptions.OneLine)]
		public Dictionary<T, U> Elements = new Dictionary<T, U>();
		
		/// <summary>
		/// Operator [] for direct access
		/// </summary>
		/// <param name="key"></param>
		public U this[T key]
		{
			get
			{
				return Elements[key];
			}
		}
		
		public static implicit operator Dictionary<T, U>(ExtendedEnum<T, U> extendedEnum)
		{
			return extendedEnum.Elements;
		}
	}
}
