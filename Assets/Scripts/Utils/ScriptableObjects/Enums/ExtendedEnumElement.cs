﻿using UnityEngine;

namespace ScriptableObjects.Enums
{
	/// <inheritdoc/>
	/// <summary>
	/// Abstract class allowing generic parameter type constraints on ExtendedEnum
	/// </summary>
	public abstract class AExtendedEnumElement : ScriptableObject {}
	
	/// <inheritdoc/>
	/// <summary>
	/// Enum element defined by a text label and a value
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class ExtendedEnumElement<T> : AExtendedEnumElement
	{
		public string Label;
		public T Value;
	}
}
