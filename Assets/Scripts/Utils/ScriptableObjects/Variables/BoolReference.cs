﻿using System;

namespace Variables
{
	[Serializable]
	public class BoolReference : Reference<bool>
	{
		public BoolVariable VariableReference;

		public override Variable<bool> Variable => VariableReference;
	}
}
