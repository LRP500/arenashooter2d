﻿using UnityEditor;

namespace Variables.Editor
{
    [CustomPropertyDrawer(typeof(Vector3Reference))]
    public class Vector3ReferenceDrawer : ReferenceDrawer
    {
    }
}
