﻿using UnityEditor;

namespace Variables.Editor
{
    [CustomPropertyDrawer(typeof(StringReference))]
    public class StringReferenceDrawer : ReferenceDrawer
    {
    }
}
