﻿using UnityEditor;

namespace Variables.Editor
{
    [CustomPropertyDrawer(typeof(FloatReference))]
    public class FloatReferenceDrawer : ReferenceDrawer
    {
    }
}
