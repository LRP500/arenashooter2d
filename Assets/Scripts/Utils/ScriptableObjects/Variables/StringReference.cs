﻿using System;

namespace Variables
{
	[Serializable]
	public class StringReference : Reference<string>
	{
		public StringVariable VariableReference;

		public override Variable<string> Variable => VariableReference;
	}
}
