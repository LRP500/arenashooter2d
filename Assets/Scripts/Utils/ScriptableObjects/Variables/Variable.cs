﻿using UnityEngine;

namespace Variables
{
	public abstract class Variable<T> : ScriptableObject
	{
	#if UNITY_EDITOR
		[Multiline]
		public string DeveloperDescription;
	#endif
		
		public T Value;
		
		public virtual void SetValue(T value)
		{
			Value = value;
		}

		public virtual void SetValue(Variable<T> variable)
		{
			Value = variable.Value;
		}
				
		public static implicit operator T(Variable<T> variable)
		{
			return variable == null ? default(T) : variable.Value;
		}
	}
}