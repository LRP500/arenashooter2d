﻿using System;

namespace Variables
{
	[Serializable]
	public class FloatReference : Reference<float>
	{
		public FloatVariable VariableReference;

		public override Variable<float> Variable => VariableReference;
	}
}