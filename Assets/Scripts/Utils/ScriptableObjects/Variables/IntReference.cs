﻿using System;

namespace Variables
{
	[Serializable]
	public class IntReference: Reference<int>
	{
		public IntVariable VariableReference;

		public override Variable<int> Variable => VariableReference;
	}
}
