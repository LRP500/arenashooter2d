﻿using UnityEngine;

namespace ScriptableObjects.Sets
{
	[CreateAssetMenu(menuName = "Sets/Strings")]
	public class StringRuntimeSet : RuntimeSet<string>
	{
	}
}
