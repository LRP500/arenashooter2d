﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float _velocity;
    
    private Rigidbody2D _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    public void Fire(Vector2 direction)
    {
        _rigidbody.AddForce(direction * _velocity);
    }

    public void OnDamageableHit(Damager damager, Damageable damageable)
    {
        Destroy(gameObject);
    }
    
    public void OnNonDamageableHit(Damager damager)
    {
        Destroy(gameObject);
    }
}
