﻿using Extensions;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private ParticleSystem _bloodEffect;
    [SerializeField] private GameObject _deathAnimationPrefab;

    private void Awake()
    {
        _bloodEffect.Stop();
    }

    public void OnTakeDamage(Damager damager, Damageable damageable)
    {
        _bloodEffect.transform.SetY(damager.transform.position.y);
        RotateBloodEffectTowardEmitter(damager.transform.position);
        _bloodEffect.Play();
    }    
    
    public void OnDie(Damager damager, Damageable damageable)
    {
        var go = Instantiate(_deathAnimationPrefab, transform);
        go.transform.SetParent(null);
        go.GetComponent<DeathAnimation>().Trigger(damager);        
        Destroy(gameObject);
    }

    private void RotateBloodEffectTowardEmitter(Vector3 target)
    {
//        var rotation = _bloodEffect.transform.localEulerAngles;
//        rotation.x = target.x < transform.position.x ? 0 : -180;
//        _bloodEffect.transform.localEulerAngles = rotation;

        var point = target;
        point.z = _bloodEffect.transform.position.z; 
        _bloodEffect.transform.LookAt(point);
    }
}
