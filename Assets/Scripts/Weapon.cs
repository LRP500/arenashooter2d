﻿using System.Collections;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public CharacterMovement _owner;
    
    [Header("Prefabs")]
    public GameObject _projectilePrefab;
    public GameObject _shellPrefab;
    
    [Header("Pools")]
    public Transform _shellPool;
    public Transform _projectilePool;
    
    [Header("Muzzle")]
    public Transform _muzzle;
    public GameObject _muzzleFlash;

    [Space]
    public float _recoilForce;

    public void Fire()
    {
        // Bullet
        var shot = Instantiate(_projectilePrefab, _muzzle);
        shot.transform.parent = _projectilePool;
        var direction = _owner.Flipped ? Vector2.left : Vector2.right;
        shot.GetComponent<Projectile>().Fire(direction);
        
        // Recoil
        _owner.Recoil(-direction, _recoilForce);
        
        // Muzzle flash
        StartCoroutine(MuzzleFlash());

        // Shell
        var shell = Instantiate(_shellPrefab, _muzzle);
        shell.GetComponent<Shell>().SetAngle(direction);
        shell.transform.parent = _shellPool;
    }
    
    private IEnumerator MuzzleFlash()
    {
        _muzzleFlash.SetActive(true);
        yield return new WaitForSeconds(0.05f);
        _muzzleFlash.SetActive(false);
    }
}
