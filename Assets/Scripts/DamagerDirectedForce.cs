﻿using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(Damager))]
public class DamagerDirectedForce : MonoBehaviour
{
    [SerializeField] private float _force;
    [SerializeField] private bool _directionOverride;
    [SerializeField, ShowIf("_directionOverride", true, true)]
    private Vector3 _direction;

    [SerializeField] private bool _randomize;

    [SerializeField, ShowIf("_randomize", true, true)]
    private Vector2 _range;
    
    public Vector2 Calculate(Transform target)
    {
        var direction = _directionOverride ? _direction: (target.position - transform.position);
        return _randomize ? Randomize(direction) * _force : direction * _force;
    }

    private Vector3 Randomize(Vector3 direction)
    {
        var randX = Random.Range(-_range.x, _range.x);
        var randY = Random.Range(-_range.y, _range.y);
        return direction + new Vector3(randX, randY);
    }
}
