﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CharacterMovement : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _sprite;
    [SerializeField] private Animator _animator;

    [SerializeField] private float _jumpForce;
    [SerializeField] private float _speedRatio;
    [SerializeField] private float _movementDamping;

    [SerializeField] private BoxCollider2D _groundCheckCollider;
    [SerializeField] private LayerMask _groundLayer;
    
    private Rigidbody2D _rigidbody;
    
    private bool _grounded;
    private bool _isRunning;

    public bool Flipped { get; private set; }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        _grounded = GroundCheck();
        _animator.SetBool("IsJumping", !_grounded);
    }

    public void Jump()
    {
        if (_grounded)
        {
            _rigidbody.AddForce(new Vector2(0f, _jumpForce));
            _grounded = false;
        }
    }

    public void Move(float horizontal)
    {
        // Velocity
        var velocity = new Vector3(horizontal * _speedRatio, _rigidbody.velocity.y);

        // Movement
        _rigidbody.velocity = Vector3.SmoothDamp(
            _rigidbody.velocity,
            velocity, ref velocity,
            _movementDamping);

        // Animator
        _isRunning = Math.Abs(_rigidbody.velocity.x) > 0.01;
        _animator.SetBool("IsRunning", _isRunning);
        
        // Flip
        if (horizontal > 0 && Flipped) Flip();
        else if (horizontal < 0 && !Flipped) Flip();
    }

    private void Flip()
    {
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
        Flipped = !Flipped;
    }

    private bool GroundCheck()
    {
        return _groundCheckCollider.IsTouchingLayers(_groundLayer);
    }

    public void Recoil(Vector2 direction, float force)
    {
        _rigidbody.AddForce(direction * force);
    }
    
}
