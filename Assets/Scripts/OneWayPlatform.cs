﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(PlatformEffector2D))]
public class OneWayPlatform : MonoBehaviour
{
    public float _offsetTime = 2f;
    
    private PlatformEffector2D _platform;
    private BoxCollider2D _collider;
    
    private void Awake()
    {
        _platform = GetComponent<PlatformEffector2D>();
        _collider = GetComponent<BoxCollider2D>();
    }

    private void Update()
    {         
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            StartCoroutine(DeactivatePlatform());
        }
    }

    private IEnumerator DeactivatePlatform()
    {
        _collider.enabled = false;
        _platform.rotationalOffset = 180f;
        yield return new WaitForSeconds(_offsetTime);
        _collider.enabled = true;
        _platform.rotationalOffset = 0f;
    }
}
