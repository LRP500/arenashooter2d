﻿using UnityEngine;

public class Shell : MonoBehaviour
{
    [SerializeField] private float _angleOffset = 100;
    [SerializeField] private float _ejectForce;
    [SerializeField] private LayerMask _groundLayer;

    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private Collider2D _collider;
    [SerializeField] private SpriteRenderer _sprite;

    private float _angle = 90f;
    
    private void Start()
    {
        Vector3 dir = Quaternion.AngleAxis(_angle, Vector3.forward) * Vector3.right;
        
        _rigidbody.AddForce(dir * _ejectForce);
    }

    private void Update()
    {
        if (_collider.IsTouchingLayers(_groundLayer) &&
            _rigidbody.velocity.sqrMagnitude < 0.1f)
        {
            Deactivate();
        }
    }
    
    private void Deactivate()
    {
        var color = _sprite.color;
        color.a = 0.5f;
        _sprite.color = color;

        Destroy(_rigidbody);
        Destroy(_collider);
        Destroy(this);
    }

    public void SetAngle(Vector2 direction)
    {
        _angle += direction.Equals(Vector2.right) ? _angleOffset : -_angleOffset;
    }
}
