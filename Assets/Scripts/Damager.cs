﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class Damager : MonoBehaviour
{
    [Serializable]
    public class DamagableEvent : UnityEvent<Damager, Damageable>
    {}

    [Serializable]
    public class NonDamagableEvent : UnityEvent<Damager>
    {}
    
    [SerializeField] private int _damage = 1;
    [SerializeField] private Collider2D _collider;
    [SerializeField] private int _maxSimultaneousHits;
    [SerializeField] private bool _canHitTriggers;
    [SerializeField] private LayerMask _hittableLayer;
    [Space]
    
    // Events are used for related action as VFX or audio
    public DamagableEvent OnDamageableHit;
    public NonDamagableEvent OnNonDamageableHit;
    
    private ContactFilter2D _contactFilter;
    private Collider2D[] _overlapResults;

    private bool _canDamage;
    
    public int Damage => _damage;
    
    private void Awake()
    {
        _contactFilter.layerMask = _hittableLayer;
        _contactFilter.useLayerMask = true;
        _contactFilter.useTriggers = _canHitTriggers;
        _overlapResults = new Collider2D[_maxSimultaneousHits];
    }

    private void FixedUpdate()
    {
        var hits = _collider.OverlapCollider(_contactFilter, _overlapResults);

        for (int i = 0; i < hits; i++)
        {
            Damageable damageable = _overlapResults[i].GetComponent<Damageable>();
            if (damageable)
            {
                OnDamageableHit?.Invoke(this, damageable);
                damageable.TakeDamage(this);
            }
            else OnNonDamageableHit?.Invoke(this);
        }
    }

    public void EnableDamage(bool state)
    {
        _canDamage = state;
    }
}

