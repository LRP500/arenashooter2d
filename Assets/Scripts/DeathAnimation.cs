﻿using UnityEngine;

/// <summary>
/// ForceFieldExtension is needed on trigger Damager so that animation can be applied a force 
/// </summary>
public class DeathAnimation : MonoBehaviour
{
    [SerializeField] private Rigidbody2D[] _rigidbodies;
    
    public void Trigger(Damager damager)
    {
        var field = damager.GetComponent<DamagerDirectedForce>();
        
        if (field == null) return;

        foreach (var rb in _rigidbodies)
        {
            rb.AddForce(field.Calculate(transform));
        }
    }

    private void Update()
    {
        foreach (Rigidbody2D rb in _rigidbodies)
        {
            if (rb != null && rb.velocity.magnitude < .0001f)
            {
                Deactivate(rb.gameObject);
                // Destroy(rb.gameObject);
            }
        }
    }
    
    private static void Deactivate(GameObject obj)
    {
        var sprite = obj.GetComponent<SpriteRenderer>();
        var color = sprite.color;
        color.a = 0.2f;
        sprite.color = color;

        Destroy(obj.GetComponent<Collider2D>());
        Destroy(obj.GetComponent<Rigidbody2D>());
    }
}
