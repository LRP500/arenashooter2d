﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Variables;

public class Damageable : MonoBehaviour
{
    [Serializable]
    public class DamageEvent : UnityEvent<Damager, Damageable>
    {}

    [SerializeField] private FloatReference _maxHealth;
    [SerializeField] private FloatReference _currentHealth;

    public DamageEvent OnTakeDamage;
    public DamageEvent OnDie;

    private bool IsDamageable { get; set; }

    public float MaxHealth => _maxHealth.Value;
    public float CurrentHealth => _currentHealth.Value;
    
    public void TakeDamage(Damager damager)
    {
        if (_currentHealth <= 0) return;

        _currentHealth.Value -= damager.Damage;
        OnTakeDamage?.Invoke(damager, this);

        if (_currentHealth <= 0)
        {
            OnDie?.Invoke(damager, this);
        }
    }

    public void SetHealth(float value)
    {
        _currentHealth.Value = _maxHealth.Value = value;
    }

    public void SetDamageable(bool state)
    {
        IsDamageable = state;
    }
}
