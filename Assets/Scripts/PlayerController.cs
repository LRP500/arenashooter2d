﻿using UnityEngine;

[RequireComponent(typeof(CharacterMovement))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private Weapon _weapon;
    
    private CharacterMovement _motor;
    
    private Vector2 _axisInput;
    private bool _jumpInput;

    private void Awake()
    {
        _motor = GetComponent<CharacterMovement>();
    }

    private void Update()
    {        
        _axisInput = new Vector2(
            Input.GetAxisRaw("Horizontal"),
            Input.GetAxisRaw("Vertical"));
        
        if (!_jumpInput) _jumpInput = Input.GetKeyDown(KeyCode.UpArrow);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            _weapon.Fire();
        }
    }

    private void FixedUpdate()
    {
        if (_jumpInput)
        {
            _motor.Jump();
            _jumpInput = false;
        }
        _motor.Move(_axisInput.x * Time.fixedDeltaTime);        
    }
}
